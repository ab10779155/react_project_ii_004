import {  useState }  from "react";
import {useHistory } from "react-router-dom";
import { usersData } from "../../../mock-data/login-mock-data";
import { TUser } from "../../hooks/types";
import {useLocalStorage} from '../../hooks/use-local-storage';
import { TIdentification } from "./types/identification";


export const useIdentification = (): TIdentification => {

  const history = useHistory();

  const [error, setError] = useState("");
  const {setValue} = useLocalStorage(); 


  const isUserInDataBase = (email: string, password: string): void => {
    const hasUser: TUser | undefined = usersData.find(({email: useEmail, password: userPassword})=> {
      return email === useEmail && password === userPassword
    })
    if (hasUser === undefined) {
      setError("Пользователь не найден, пожалуйста, обновите страницу и попробуйте еще");
    } else {
      setValue("user", { email, admin: hasUser.admin, id: hasUser.id});
      history.push("/main");
    }   
  }
  return {isUserInDataBase, error}; 
}
