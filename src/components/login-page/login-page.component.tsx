import React from "react";
import { Formik, Field, Form } from "formik";
import {BasicFormSchema} from "../../schema/BasicFormSchema";
import {useIdentification} from "./hooks/use-identification";
import { initialFormState } from "../../constants/initial-form-state.constant";
import { TInitialFormState } from "../../constants/type";
import { InputComponent } from "../input/input.component";
import { useFormik } from 'formik';

export const LoginPageComponent = () => {

  const {isUserInDataBase, error} = useIdentification();

  const handleSubmit = (values: TInitialFormState): void => {
    isUserInDataBase(values.email, values.password);
  };

  const formik = useFormik({
    initialValues: initialFormState,
    validationSchema: BasicFormSchema,
    onSubmit: handleSubmit,
  })

  return (
    <div className="form-component">
      <h1 className="form-header">Login form</h1>
      <span className="error-text">{error}</span>
      <form className="form-component" onSubmit={formik.handleSubmit} noValidate >
          <div className="field-container">
            <label htmlFor="email" className="input-wrapper">Email</label>
            <InputComponent 
              value={formik.values.email}
              onChange={formik.handleChange} 
              onBlur={formik.handleBlur} 
              name="email" 
              className="input input-login" 
              id="userEmail"
              type="email"
              placeholder="введите e-mail"
            />
          </div>
        
          {formik.touched.email && 
            formik.errors.email ? <span className="error-text">{formik.errors.email}</span> : null}

          <div className="field-container">
            <label htmlFor="password" className="input-wrapper">Password</label>
            <InputComponent 
              value={formik.values.password} 
              onChange={formik.handleChange} 
              onBlur={formik.handleBlur} 
              name="password" 
              className="input input-password" 
              id="userPassword"
              type="password"
              placeholder="введите пароль"
            />
          </div>  

          {formik.touched.password && 
            formik.errors.password ? <span className="error-text">{formik.errors.password}</span> : null}

          <div className="btn-container">
            <button type="reset" className="btn-reset" >reset</button>
            <button type="submit"className="btn-submit">Login in</button>
          </div>  
      </form>
    </div>
  )
  };