import React from 'react';
import { Switch, Route} from "react-router-dom";
import {MainPageComponent} from "../components/main-page/main-page.component";
import { LoginPageComponent } from './login-page/login-page.component';
import { useUserLoggedIn } from './hooks/use-user-logged-in';

export const App = () => {

useUserLoggedIn();

  return (
          <Switch>
              <Route component={MainPageComponent} path="/main"/>
              <Route component={LoginPageComponent} path="/" exact />  
          </Switch>
      
  );
};

