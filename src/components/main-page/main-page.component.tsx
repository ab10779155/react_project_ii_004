import React  from "react";
import { useHistory } from "react-router-dom";
import {useLocalStorage} from '../../components/hooks/use-local-storage';


export const MainPageComponent = () => {
    const history = useHistory();
    const {removeValue} = useLocalStorage(); 

    const removeKeyFromLocale = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
        removeValue("user");
        history.push("/");
    }

    return (
    <>
        <h2 className="main-page">Main page</h2>
        <button className="btn-reset" type="button" onClick={removeKeyFromLocale}
        >
            Log out
        </button>
    </>
    )
}

