import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { TUserLoggedIn } from "./types/user-logged-in";
import { useLocalStorage } from "./use-local-storage";


export const useUserLoggedIn = (): TUserLoggedIn => {

     const [loggedIn, setloggedIn] = useState(false); 

     const {getValue, state} = useLocalStorage();
     const history = useHistory();

     useEffect(() => {
          getValue("user");
     }, []);

     useEffect(() => {
          if (state) {   
               history.push("/main");
               setloggedIn(true);
          } else {
               history.push("/");
               setloggedIn(false);
          }
     }, [state]);

     return {loggedIn}
}