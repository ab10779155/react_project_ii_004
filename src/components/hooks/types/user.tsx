export type TUser = {
    email: string;
    admin: boolean;
    id: string;
};