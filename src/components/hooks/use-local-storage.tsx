import {TUser} from "./types/user"
import {TLocalStorage} from "./types/local-storage"
import {  useState } from "react";
import { TSetValue } from "./types";

export  const useLocalStorage = (): TLocalStorage => { 

    const [state, setState] = useState<string | null | void>(null); 
    

    const hasValue = (key: string): string | null => {
        return  localStorage.getItem(key);
    }

    const getValue = (key: string): void  => {
        const value: string | null = hasValue(key);
        if (value) {
            setState(localStorage.getItem(key));
        } else {
            setState(null);
        }
    }

    const setValue:TSetValue<string, TUser> = (key: string, data: TUser): void  => {
        localStorage.setItem(key, JSON.stringify(data));

    }

    const removeValue = (key: string): void => {
        const value: string | null = hasValue(key);
        if (value) {
            setState(localStorage.removeItem(key));
        } else {
            setState(null);
        }
    }
    return {state, hasValue, setValue, getValue, removeValue}
};

